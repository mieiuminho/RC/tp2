## Exercício 2 {#sec:ex2}

### a)

**Qual é o endereço IP da interface ativa do seu computador?**

![Endereço IP da interface ativa do computador](figures/part1/source_ip.png)

O endereço da interface ativa do nosso computador é: `192.168.100.230`.

### b)

**Qual é o valor do campo protocolo? O que identifica?**

![Campo do protocolo de rede](figures/part1/protocol_field.png)

O valor do campo do protocolo é ICMP(1) e identifica o protocolo de internet
(_Internet Protocol_).

### c)

**Quantos bytes tem o cabeçalho IP(v4)? Quantos bytes tem o campo de
dados (payload) do datagrama? Como se calcula o tamanho do payload?**

![Tamanho (em _bytes_) do cabeçalho e do total do pacote de
rede](figures/part1/header_total_length.png)

O cabeçalho tem 20 bytes e o datagrama tem 40 bytes de _payload_. Para calcular
o número de bytes de _payload_ basta subtrair o número de bytes de _header_ ao
número de bytes total que o datagrama ocupa.

### d)

**O datagrama IP foi fragmentado? Justifique.**

![O pacote não foi fragmentado](figures/part1/non_fragmentation_proof.png)

O datagrama não foi fragmentado, uma vez que o campo _Fragment offset_ tem o
valor 0.

### e)

**Ordene os pacotes capturados de acordo com o endereço IP fonte (e.g.,
selecionando o cabeçalho da coluna Source), e analise a sequência de
tráfego ICMP gerado a partir do endereço IP atribuído à interface da sua
máquina. Para a sequência de mensagens ICMP enviadas pelo seu
computador, indique que campos do cabeçalho IP variam de pacote para
pacote.**

Os campos do cabeçalho IP que variam de pacote para pacote são: _TTL_
(Time-To-Live) e _header checksum_.

### f)

**Observa algum padrão nos valores do campo de Identificação do
datagrama IP e TTL?**

Relativamente ao _TTL_ este segue o padrão descrito no enunciado deste trabalho
prático, começa por enviar 3 datagramas com _TTL_ igual 1, seguidos de 3
datagramas com _TTL_ igual a 2 e assim sucessivamente até ao ponto em que envia
um datagram com _TTL_ igual a 6.  O campo de identificação incrementa uma
unidade por cada datagrama enviado, uma vez que não ocorre fragmentação.

### g)

**Ordene o tráfego capturado por endereço destino e encontre a série de
respostas ICMP TTL exceeded enviadas ao seu computador. Qual é o
valor do campo TTL? Esse valor permanece constante para todas as
mensagens de resposta ICMP TTL exceeded enviados ao seu host?
Porquê?**

![Datagramas com _TTL_ expired](figures/part1/ttl_expired.png)

![Expansão dos datagramas com _TTL_ igual a 64](figures/part1/ttl64.png)

![Expansão dos datagramas com _TTL_ igual a 254](figures/part1/254.png)

Para as mensagens com _TTL_ expired obtivemos dois valores de _TTL_ diferentes:
64 e 254. Verificamos, por isso, que o valor não permanece constante, no
entanto, verificamos que existem 3 mensagens com _TTL_ igual a 64 e 3 mensagens
com _TTL_ igual a 254.

Após alguma pesquisa, conseguimos averiguar que 64 é o _TTL_ predefinido para
respostas em máquinas Linux, pelo que se encontra justificada a existência 3
mensagens com esse valor. As mensagens que apresentam _TTL_ igual a 254
passam por mais uma interface do que as anteriores, pelo que na pesquisa
efetuada usamos um _TTL_ de 255, tendo constatado que este é o valor padrão
usado na resposta por routers _CISCO_.
