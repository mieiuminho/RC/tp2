## Exercício 2 {#sec:ex2p}

**Para o router e um laptop do departamento B.**

### a)

**Execute o comando netstat –rn por forma a poder consultar a tabela de
encaminhamento unicast (IPv4). Inclua no seu relatório as tabelas de encaminhamento
obtidas; interprete as várias entradas de cada tabela. Se necessário, consulte o manual
respetivo (man netstat).**

![`netstat` router do departamento B](figures/part2/netstatRouterB.png){
height=120px; width=400px }

![`netstat` portátil do departamento B](figures/part2/netstatPortatilB.png){
width=400px }

A tabela de encaminhamento do router do departamento B tem 10 entradas. No caso
geral, uma entrada da tabela significa que para que o pacote alcance a rede de
endereço `Destino` pela interface local `Iface` deve seguir pela interface
de endereço `Router`.

Existe um caso diferente de todos os outros na tabela de encaminhamento do
router B: a primeira entrada que tem como `Roteador`: `0.0.0.0`. Isto acontece
porque o endereço do `Destino` e da rede onde o pacote se encontra são o mesmo
(o pacote já se encontra na rede `10.0.0.0`), pelo que não necessário definir o
`Roteador` para onde será efetuado o próximo "jump".

### b)

**Diga, justificando, se está a ser usado encaminhamento estático ou dinâmico
(sugestão: analise que processos estão a correr em cada sistema).**

Está a ser usado encaminhamento dinâmico, uma vez que o programa _ospf_ está em
correr em _background_ no router do departamento B, por forma a escolher
dinamicamente o caminho mais curto para cada pacote entre o router e o portátil.

### c)

**Admita que, por questões administrativas, a rota por defeito (0.0.0.0 ou
default) deve ser retirada definitivamente da tabela de encaminhamento do
servidor S1 localizado no departamento A. Use o comando route delete para o
efeito. Que implicação tem esta medida para os utilizadores da empresa que
acedem ao servidor? Justifique.**

Os pacotes que tiverem como destino a sub-rede A não sofrem alterações no
encaminhamento. No caso dos pacotes terem qualquer outro destino são
descartados, uma vez que não encaixam em nenhuma entrada da tabela (caso
existisse a rota _default_ eram enviados por essa).

### d)

**Adicione as rotas estáticas necessárias para restaurar a conectividade para o
servidor S1 por forma a contornar a restrição imposta na alínea c). Utilize para
o efeito o comando route add e registe os comandos que usou.**

Este é o comando geral para adicionar entradas à tabela de encaminhamento:

`route add -net <endereco_destino> netmask 255.255.255.0 <endereco_roteador> eth0`

Temos, então, de definir estaticamente a rota entre o servidor e as sub-redes.
Assim para cada sub-rede temos uma  entrada:

`route add -net 10.0.0.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.1.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.2.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.3.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.5.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.6.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.7.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.8.0 netmask 255.255.255.0 eht0`

`route add -net 10.0.9.0 netmask 255.255.255.0 eht0`


### e)

**Teste a nova política de encaminhamento garantindo que o servidor está
novamente acessível utilizando para o efeito o comando ping. Registe a nova
tabela de encaminhamento do servidor.**

![Nova tabela de encaminhamento do servidor
1](figures/part2/encaminhamentoS1.png){ height=120px; width=400px }

![`ping` de um portátil do departamento A para o
servidor](figures/part2/p1A_after.png){ width=400px }

![`ping` de um portátil do departamento B para o
servidor](figures/part2/p1B_after.png){ width=400px }

![`ping` de um portátil do departamento C para o
servidor](figures/part2/p1A_after.png){ width=400px }

![`ping` de um portátil do departamento D para o
servidor](figures/part2/p1A_after.png){ width=400px }

Como para todos os portáteis os 5 pacotes são transferidos para o servidor com
sucesso e os 5 pacotes enviados pelo servidor são recebidos com sucesso,
conseguimos atestar a validades das rotas estáticas estabelecidas.
