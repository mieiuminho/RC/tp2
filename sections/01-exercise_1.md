# Parte 1

## Exercício 1 {#sec:ex1}

**Prepare uma topologia no CORE para verificar o comportamento do traceroute.
Ligue um host (servidor) s1 a um router r2; o router r2 a um router r3, o router
r3 a um router r4, que por sua vez, se liga a um host (pc) h5. (Note que pode
não existir conectividade IP imediata entre s1 e h5 até que o routing
estabilize). Ajuste o nome dos equipamentos atribuídos por defeito para a
topologia do enunciado.**

### a)

**Active o wireshark ou o tcpdump no pc s1. Numa shell de s1, execute o
comando traceroute -I para o endereço IP do host h5.**

![Topologia no CORE](figures/part1/exercicio1_setup.png)

![Resultado da execução de `traceroute` na shell do servidor para o
computador](figures/part1/traceroute_1.1_a.png)

### b)

**Registe e analise o tráfego ICMP enviado por s1 e o tráfego ICMP
recebido como resposta. Comente os resultados face ao comportamento
esperado.**

![Tráfego ICMP enviado pelo servidor
1](figures/part1/exercicio1_analise_trafego.png)

Sabemos, conforme é dito no enunciado, que o programa `traceroute` opera
enviando inicialmente um ou mais datagramas com o _TTL_ (Time-To-Live) igual a
1; seguidamente envia um ou mais datagramas com _TTL_ igual a 2; depois com
_TTL_ igual a 3 e assim sucessivamente.

Neste exercício conseguimos constatar que o `tracerout` opera, de facto, da
forma descrita. Quando executamos numa shell do servidor 1 o comando:
`traceroute -I 10.0.3.10`, sendo este último o endereço de IP do servidor número
5, conseguimos perceber que são enviados a partir do servidor 1 três datagramas
com _TTL_ igual a 1, três datagramas com _TTL_ igual a 2 e ainda três datagramas
com _TTL_ igual a 3, sendo que nenhum destes alcança, com sucesso, o servidor 5.
O servidor 5 envia depois uma resposta ao pedido do servidor 1 (uma resposta por
pedido, perceba-se).

### c)

**Qual deve ser o valor inicial mínimo do campo TTL para alcançar o
destino h5? Verifique na prática que a sua resposta está correta.**

Conforme se deduz muito facilmente pela resposta anterior, o _TTL_ mínimo para
que datagramas enviados a partir do servidor 1 alcancem o servidor 5 é 4, uma
vez que a "distância" que os separa é 4 saltos (_hops_).

Como podemos constatar na imagem acima os pedidos começam a receber resposta
assim que começam a ser enviados com _TTL_ igual a 4, o que corrobora a resposta
dada.

### d)

**Qual o valor médio do tempo de ida-e-volta (Round-Trip Time) obtido?**

Através da análise do output do _Wireshark_ depois da execução do comando
pedido, verificamos que existem 7 pedidos (_requests_) do servidor 1 que
recebem resposta por parte do servidor 5, desta forma, podemos utilizar as
diferenças entre o instante de envio do pedido e o instante em que é recebida
a resposta para calcular a média do tempo de ida e volta (_Round-Trip Time_).
Para cada pedido temos que o tempo de ida e volta é: $\Delta t_{1} = t_{f} -
t_{i}$, sendo $t_{f}$ o instante em que a resposta ao pedido chegou ao
servidor 1 e $t_{i}$ o instante em que o pedido saiu do servidor 1. Assim
temos:

$\Delta t_{1} = 4.120369077 - 4.120342273 = 0.000026804$

$\Delta t_{2} = 4.120393033 - 4.120375959 = 0.000017074$

$\Delta t_{3} = 4.120415125 - 4.120398551 = 0.000016574$

$\Delta t_{4} = 4.120437761 - 4.120421933 = 0.000015828$

$\Delta t_{5} = 4.120459826 - 4.120443266 = 0.000015460$

$\Delta t_{6} = 4.120481915 - 4.120465193 = 0.000016698$

$\Delta t_{7} = 4.120504348 - 4.120488381 = 0.000015967$

$\bar{t_{i}} = 0.000017929$

O tempo médio de ida e volta (_Round-Trip Time_) é 0.000017929 segundos.
