## Exercício 3 {#sec:ex3}

**Pretende-se agora analisar a fragmentação de pacotes IP. Reponha a ordem do
tráfego capturado usando a coluna do tempo de captura. Observe o tráfego
depois do tamanho de pacote ter sido definido para 42XX bytes.**

O tamanho definido para as mensagens foi de 4207, uma vez que somos o grupo
número 7 do nosso turno.

### a)

**Localize a primeira mensagem ICMP. Porque é que houve necessidade de
fragmentar o pacote inicial?**

Devido ao tamanho que os pacotes agora configuram (4207 _bytes_) não é possível
transmiti-los de uma só vez, uma vez que excedem o máximo de bytes que é
transmitido transportar por pacote.

### b)

**Imprima o primeiro fragmento do datagrama IP segmentado. Que
informação no cabeçalho indica que o datagrama foi fragmentado? Que
informação no cabeçalho IP indica que se trata do primeiro fragmento?
Qual é o tamanho deste datagrama IP?**

![Informações do primeiro fragmento](figures/part1/fragmented_proof.png)

O pacote em questão foi fragmentado, como podemos confirmar pelo facto do campo
_More fragments_ ter valor 1. Sabemos que este é o primeiro dos fragmentos pelo
facto de o campo _Fragment offset_ ter valor 0.  O tamanho total do datagrama
que foi fragmentado é 4207 _bytes_, enquanto que o tamanho deste fragmento é de
1500 _bytes_.

### c)

**Imprima o segundo fragmento do datagrama IP original. Que informação
do cabeçalho IP indica que não se trata do 1º fragmento? Há mais
fragmentos? O que nos permite afirmar isso?**

![Informações do segundo fragmento](figures/part1/fragment2_info.png)

Conseguimos perceber que este não é o primeiro segmento pelo facto do campo
_Fragment offset_ ser igual a 1480. Sabemos ainda que existem mais fragmentos,
pelo facto de _More fragments_ ter valor 1.

### d)

**Quantos fragmentos foram criados a partir do datagrama original? Como
se detecta o último fragmento correspondente ao datagrama original?**

São criados 3 fragmentos por cada pacote. Detetamos o último pacote pelo facto
de este estar ao associado ao outros dois e ter o campo _More fragments_ igual a
0. Constatamos ainda que o os dois fragmentos anteriores referenciam (no
   _Wireshark_) este último com a seguinte frase: [Reassembled in #58] (neste
   caso número 58, no caso de outros pacotes fragmentos seria o número que
   identifica o último fragmento).

### e)

**Indique, resumindo, os campos que mudam no cabeçalho IP entre os
diferentes fragmentos, e explique a forma como essa informação permite
reconstruir o datagrama original.**

Entre fragmentos do mesmo pacote variam: _Fragment offset_ e _More fragments_.
Através do campo _Fragment offset_ conseguimos reconstruir o pacote (com uma
simples ordenação crescente dos _Fragment offset_ conseguimos restituir a ordem
dos fragmentos do pacote e consequentemente reconstruí-lo). Com o campo _More
fragments_ sabemos quando parar de construir o pacote atual e passar para o
próximo, uma vez que quando _More fragments_ for igual a 0 é porque não existem
mais fragmentos do presente pacote.
