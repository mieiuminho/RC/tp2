## Exercício 3 {#sec:ex3p}

**Por forma a minimizar a falta de endereços IPv4 é comum a utilização de
sub-redes. Além disso, a definição de sub-redes permite uma melhor
organização do espaço de endereçamento das redes em questão.**

**Para definir endereços de sub-rede é necessário usar a parte prevista para
endereçamento de host, não sendo possível alterar o endereço de rede
original. Recorda-se que o subnetting, ao recorrer ao espaço de endereçamento
para host, implica que possam ser endereçados menos hosts. Considere a
topologia definida anteriormente. Assuma que o endereçamento entre os routers
se mantém inalterado, contudo, o endereçamento em cada departamento deve ser
redefinido.**

### 1)

**Considere que dispõe apenas do endereço de rede IP 172.yyx.32.0/20, em que
“yy” são os dígitos correspondendo ao seu número de grupo (Gyy) e “x” é o
dígito correspondente ao seu turno prático (PLx). Defina um novo esquema de
endereçamento para as redes dos departamentos (mantendo a rede de acesso e
core inalteradas) e atribua endereços às interfaces dos vários sistemas
envolvidos. Deve justificar as opções usadas.**

![Topologia do CORE com os novos endereços IP](figures/part2/core-ipsnovos.png)

O endereço de IP fornecido (`172.72.32.0/20`) tem máscara 20, portanto podemos
usar 12 bits para definir sub-redes e hosts. Uma vez que temos que endereçar 5
sub-redes (servidor 1, departamento A, departamento B, departamento C e
departamento D) escolhemos utilizar 3 desses bits para identificar a sub-rede.
Com 3 bits podia pensar-se que conseguiríamos identificar 8 sub-redes, no
entanto os padrões de bits (`000` e `111`) estão reservados.

Para identificar a sub-rede do departamento A escolhemos usar o endereço:
`172.72.34.0`, para a sub-rede do departamento B o endereço: `172.72.36.0`, para
a sub-rede do departamento C o endereço: `172.72.38.0`, para a sub-rede do
departamento D: `172.72.40.0` e para a sub-rede do servidor 1: `172.72.42.0`.

### 2)

**Qual a máscara de rede que usou (em notação decimal)? Quantos interfaces IP
pode interligar em cada departamento? Justifique.**

A máscara de rede que utilizamos é 23 (em notação decimal).
Como sobram 9 bits para endereçar interfaces, dentro de cada sub-rede
conseguimos interligar ($2^{9}$ - 2) interfaces, ou seja, 510 interfaces.

### 3)

**Garanta e verifique que a conectividade IP entre as várias redes locais da
organização MIEI-RC é mantida. Explique como procedeu.**

![`ping` de um portátil do departamento A para um portátil do departamento
B](figures/part2/A_B.png)

![`ping` de um portátil do departamento A para um portátil do departamento
C](figures/part2/A_C.png)

![`ping` de um portátil do departamento A para um portátil do departamento
D](figures/part2/A_D.png)

![`ping` de um portátil do departamento B para um portátil do departamento
C](figures/part2/B_C.png)

![`ping` de um portátil do departamento B para um portátil do departamento
D](figures/part2/B_D.png)

![`ping` de um portátil do departamento C para um portátil do departamento
D](figures/part2/C_D.png)

![`ping` do servidor 1 para um portátil do departamento A](figures/part2/S_A.png)

![`ping` do servidor 1 para um portátil do departamento B](figures/part2/S_B.png)

![`ping` do servidor 1 para um portátil do departamento C](figures/part2/S_C.png)

![`ping` do servidor 1 para um portátil do departamento D](figures/part2/S_D.png)

Os `ping` realizados configuram todas as combinações possíveis de fonte e
destino de um datagrama. Como os `ping` são todos bem sucedidos fica garantida a
conectividade IP entre todas as sub-redes.
