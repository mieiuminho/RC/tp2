# Conclusão {#sec:con}

A realização deste trabalho prático foi extremamente elucidativa, contribuindo
para um melhor entendimento de todo o capítulo referente ao protocolo IPv4.

Em virtude do desenvolvimento desta atividade melhoramos o conhecimento, de
forma empírica, de diversas componentes dos datagramas IP. Relativamente à
componente do cabeçalho dos datagramas: _TTL_ (Time-To-Live), percebemos que uma
má definição pode resultar num pacote que fica à "deriva" na rede (ou seja, não
encontra a rede com o endereço destino e tem um _TTL_ exagerado, pelo que
circula pela rede até que _TTL_ seja igual a 0). Por outro lado, um _TTL_
demasiado pequeno pode resultar num descartamento precoce do datagrama IP. Foi
possível também constatar que, conforme lecionado nas aulas práticas, os
datagramas IP têm duas partes: o cabeçalho e o corpo (normalmente denominado
por _payload_). Durante a realização deste trabalho constatamos que o cabeçalho
contém informação fulcral para a correta interpretação do datagrama: o tamanho
do mesmo, o bit que permite saber se este foi fragmentado, o _fragment offset_
que, caso o datagrama seja fragmentado, permite reconstrui-lo corretamente.
Tivemos o contacto com a fragmentação dos datagramas IP que acontece quando os
estes têm tamanho superior àquele que é possível transmitir, tendo assim de
ser divididos para que possam ser reconstruídos no destino de modo a ter acesso
ao datagrama original.

O contacto com tabelas de encaminhamento foi muito importante quer pela
observação e interpretação quer pela manipulação que fizemos, na qual
construímos rotas estáticas para o trânsito de pacotes entre um servidor e uma
rede (constituída por sub-redes). Percebemos também que existem programas que
correm nos nodos da rede de forma a perceber em cada etapa qual o próximo passo
a tomar, constituindo este mecanismo a base do encaminhamento dinâmico.

Por último, tivemos a oportunidade de colocar em prática os conhecimentos de
_subnetting_ que possuímos, uma vez que tivemos de endereçar a partir de um
endereço base, 5 sub-redes, de maneira a que seja possível identificar diversas
interfaces em cada uma dessas redes secundárias.
