# Parte 2

## Exercício 1 {#sec:ex1p}

![Tipologia CORE](figures/part2/tipologia_core.png)

Durante a elaboração da topologia surgiram dúvidas relativamente ao
posicionamento do servidor 1 (se devia estar conectado ao switch do departamento
A ou ao router do departamento A) e após esclarecimento do docente prosseguimos
assumindo a conexão direta ao router do departamento A.

### a)

**Indique que endereços IP e máscaras de rede foram atribuídos pelo CORE a
cada equipamento. Para simplificar, pode incluir uma imagem que ilustre de
forma clara a topologia definida e o endereçamento usado.**

#### Routers

* **Router A**

  Este router tem 4 interfaces, uma vez que se encontra diretamente ligado a
  4 sub-redes (router B, router C, _ethernet switch_ `sA` e servidor 1).

  Para a interface que se liga com a o router B, temos:

  - Endereço de IP: 10.0.0.1/24, sendo que a máscara é /24.

  Para a interface que se liga com a o router C, temos:

  - Endereço de IP: 10.0.1.1/24, sendo que a máscara é /24.

  Para a interface que se liga com o _ethernet switch_ `sA`, temos:

  - Endereço de IP: 10.0.5.1/24, sendo que a máscara é /24.

  Para a interface que se liga com o servidor 1, temos:

  - Endereço de IP: 10.0.4.1/24, sendo que a máscara é /24.



* **Router B**

  Este router tem 3 interfaces, uma vez que se encontra diretamente ligado a
  3 sub-redes (router A, router D e ao _ethernet switch_ `sB`).

  Para a interface que se liga com a o router A, temos:

  - Endereço de IP: 10.0.0.2/24, sendo que a máscara é /24.

  Para a interface que se liga com a o router D, temos:

  - Endereço de IP: 10.0.2.1/24, sendo que a máscara é /24.

  Para a interface que se liga com o _ethernet switch_ `sB`, temos:

  - Endereço de IP: 10.0.8.1/24, sendo que a máscara é /24.


* **Router C**

  Este router tem 3 interfaces, uma vez que se encontra diretamente ligado a
  3 sub-redes (router A, router D e ao _ethernet switch_ `sC`).

  Para a interface que se liga com a o router A, temos:

  - Endereço de IP: 10.0.1.2/24, sendo que a máscara é /24.

  Para a interface que se liga com a o router D, temos:

  - Endereço de IP: 10.0.3.1/24, sendo que a máscara é /24.

  Para a interface que se liga com o _ethernet switch_ `sC`, temos:

  - Endereço de IP: 10.0.6.1/24, sendo que a máscara é /24.



* **Router D**

  Este router tem 4 interfaces, uma vez que se encontra diretamente ligado a
  4 sub-redes (router B, router C, _ethernet switch_ `sD` e router exterior).

  Para a interface que se liga com a o router B, temos:

  - Endereço de IP: 10.0.2.2/24, sendo que a máscara é /24.

  Para a interface que se liga com a o router C, temos:

  - Endereço de IP: 10.0.3.2/24, sendo que a máscara é /24.

  Para a interface que se liga com o _ethernet switch_ `sD`, temos:

  - Endereço de IP: 10.0.7.1/24, sendo que a máscara é /24.

  Para a interface que se liga com o router exterior, temos:

  - Endereço de IP: 10.0.9.1/24, sendo que a máscara é /24.


* **Router Exterior**

  Este router tem 1 interface, uma vez que se encontra diretamente ligado a
  1 sub-rede (router D).

  Para esta interface temos:

  - Endereço de IP: 10.0.9.2/24, sendo que a máscara é /24.


#### Portáteis do departamento A

 Os endereços de IP destes equipamentos são bastante parecidos (tendo os três
 primeiro bytes iguais), diferindo, por isso, apenas no último byte, conforme
 mostramos:

* `p1A`

  - Endereço de IP: 10.0.5.20/24, sendo que a máscara é /24.

* `p2A`

  - Endereço de IP: 10.0.5.21/24, sendo que a máscara é /24.

* `p3A`

  - Endereço de IP: 10.0.5.22/24, sendo que a máscara é /24.


#### Portáteis do departamento B

Os endereços de IP destes equipamentos são bastante parecidos (tendo os três
primeiro bytes iguais), diferindo, por isso, apenas no último byte, conforme
mostramos:

* `p1B`

  - Endereço de IP: 10.0.8.20/24, sendo que a máscara é /24.

* `p2B`

  - Endereço de IP: 10.0.8.21/24, sendo que a máscara é /24.

* `p3B`

  - Endereço de IP: 10.0.8.22/24, sendo que a máscara é /24.


#### Portáteis do departamento C

Os endereços de IP destes equipamentos são bastante parecidos (tendo os três
primeiro bytes iguais), diferindo, por isso, apenas no último byte, conforme
mostramos:

* `p1C`

  - Endereço de IP: 10.0.6.20/24, sendo que a máscara é /24.

* `p2C`

  - Endereço de IP: 10.0.6.21/24, sendo que a máscara é /24.

* `p3C`

  - Endereço de IP: 10.0.6.22/24, sendo que a máscara é /24.


#### Portáteis do departamento D

Os endereços de IP destes equipamentos são bastante parecidos (tendo os três
primeiro bytes iguais), diferindo, por isso, apenas no último byte, conforme
mostramos:

* `p1D`

  - Endereço de IP: 10.0.7.20/24, sendo que a máscara é /24.

* `p2D`

  - Endereço de IP: 10.0.6.21/24, sendo que a máscara é /24.

* `p3D`

  - Endereço de IP: 10.0.6.22/24, sendo que a máscara é /24.


### b)

**Trata-se de endereços públicos ou privados? Porquê?**

Os endereços atribuídos são privados uma vez que se encontram no bloco de
endereços de IP: `10.0.0.0` a `10.255.255.255 /8`.

### c)

**Por que razão não é atribuído um endereço IP aos switches?**

Não é atribuído um endereço de IP aos _switches_ uma vez que estes não são
destinatários de pacote algum, alojam apenas tabelas de endereçamento.

### d)

**Usando o comando ping certifique-se que existe conectividade IP entre os laptops dos
vários departamentos e o servidor do departamento A (basta certificar-se da
conectividade de um laptop por departamento).**

![Departamento A](figures/part2/pAservidorA.png){ height=110px }

![Departamento B](figures/part2/pBservidorA.png){ height=110px }

![Departamento C](figures/part2/pCservidorA.png){ height=110px }

![Departamento D](figures/part2/pDservidorA.png){ height=110px }

Como os 5 pacotes enviados pelos portáteis são recebidos com sucesso pelo
servidor do departamento e os 5 pacotes enviados pelo servidor são recebidos
com sucesso por parte dos portáteis, confirmamos que existe conectividade IP
entre os vários _laptops_ dos departamentos e o servidor do departamento A.

### e)

**Verifique se existe conectividade IP do router de acesso Rext para o servidor S1.**

![Router Exterior](figures/part2/exteriorServidorA.png){ height=110px }

O facto de os pacotes serem transmitidos com sucesso entre o router exterior e o
servidor verifica que existe conectividade IP entre estes.
